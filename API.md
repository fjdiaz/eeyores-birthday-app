# Using Eeyore's Birthday API

This is the API usage guide for Eeyore's birthday! The API allows
you to:

- View a welcome message
- View the attendee list
- Add an attendee to the list

## Viewing a welcome message

```bash
$ curl localhost:8080/welcome

Hello, and welcome to Eeyore's Birthday!
```

## Viewing the attendee list

```bash
$ curl localhost:8080/attendees

[
    {
        "id": "1",
        "name": "Eeyore",
        "role": "Host"
    },
    {
        "id": "2",
        "name": "Winnie the Pooh",
        "role": "Guest"
    },
    {
        "id": "3",
        "name": "Christopher Robin",
        "role": "Guest"
    }
]
```

## Adding an attendee to the list

```bash
$ curl -X POST localhost:8080/attendees -d

```