# Eeyore's Birthday App!

Welcome to the Eeyore's birthday App Page 🥳! This app has been created in
order for you to see who is attending Eeyore's birthday as well as add new
attendees to the list.

![Donkey](donkey.jpg)

Photo by [Daniel Fazio]("https://unsplash.com/@danielfazio?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash") on [Unsplash](https://unsplash.com/photos/shallow-focus-photography-of-brown-donkey-z5Fb1V8Bi28?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash).
  

## About Eeyore's Birthday

Eeyore's Birthday Party is a day-long festival taking place annually in Austin, Texas since 1963. It includes live music, food and drink vending which benefit local non-profit organizations, attendees in colorful costumes, and very large drum circles. The event is frequented by children and families, with specific events presented for them by the event organizers. The festival is named in honor of Eeyore, a character in A. A. Milne's Winnie-the-Pooh stories. -[Wikipedia](https://en.wikipedia.org/wiki/Eeyore%27s_Birthday_Party)