FROM golang:latest

RUN apt-get install git
RUN apt-get update -y
RUN apt-get upgrade -y

WORKDIR /app
COPY . /app

RUN go mod download
RUN go build -o /app/eeyores-birthday-app

ENTRYPOINT []
CMD [ "/app/eeyores-birthday-app"]