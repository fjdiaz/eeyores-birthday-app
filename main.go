package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// attendee represents a attendee Eeyore's birthday party
type attendee struct {
	ID   string `json:"id"`
	Name string `json:"name"`
	Role string `json:"role"`
}

// list of current attendees
// Note: Lives in memory until server is stopped/restarted
var attendees = []attendee{
	{ID: "1", Name: "Eeyore", Role: "Host"},
	{ID: "2", Name: "Winnie the Pooh", Role: "Guest"},
	{ID: "3", Name: "Christopher Robin", Role: "Guest"},
}

func main() {
	router := gin.Default()
	router.GET("/welcome", welcome)
	router.GET("/attendees", getAttendees)
	router.POST("/attendees", addAttendees)

	router.Run("localhost:8080")
}

// welcome returns a welcome message for those attending Eeyore's birthday
func welcome(c *gin.Context) {
	c.String(http.StatusOK, "Hello, and welcome to Eeyore's Birthday!")
}

// getAttendees returns the attendees list in json
func getAttendees(c *gin.Context) {
	c.IndentedJSON(http.StatusOK, attendees)
}

// postAttendees adds a new attendee to the attendees list
// and returns the newly added attendee
func addAttendees(c *gin.Context) {
	var newAttendee attendee

	// Call BindJSON to bind the json received in a request
	// to the newAttendee object
	if err := c.BindJSON(&newAttendee); err != nil {
		return
	}

	// Add the new attendee to the attendees list
	attendees = append(attendees, newAttendee)

	c.IndentedJSON(http.StatusCreated, newAttendee)
}
